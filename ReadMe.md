This repo contains the task about synchronization and automating processes. In this task, the main challenge is how to check changes in master DB and sync slave with it
through an automated process. For using airflow and because of its dependencies, we used
docker containers to install it in a container to make it more usable. and created a dockerfile
for using Flask to connect it to the Airflow
