CREATE TABLE Book (
  id SERIAL PRIMARY key,
  ISBN INT NOT NULL,
  title VARCHAR,
  description VARCHAR,
  publisher VARCHAR,
  category VARCHAR,
  languages VARCHAR,
  lastUpdate timestamp,
  translatedFrom INT,
  deleted BOOLEAN,
  FOREIGN KEY (translatedFrom)
      REFERENCES Book (id)

);


CREATE TABLE Author (
  id SERIAL PRIMARY key,
  authorId INT NOT NULL,
  firstName VARCHAR,
  secondName VARCHAR,
  lastUpdate timestamp,
  deleted BOOLEAN
);

CREATE TABLE Book_Author (
  id SERIAL PRIMARY key,
  ‌Bookid INT NOT NULL,
  authorId INT not NULL,
  lastUpdate timestamp,
  deleted BOOLEAN,
  FOREIGN KEY (‌Bookid)
      REFERENCES Book (id),
  FOREIGN KEY (authorId)
      REFERENCES Author (id)
  
);

CREATE TABLE Print (
  id SERIAL PRIMARY key,
  printId INT NOT NULL,
  bookId INT not NULL,
  versions INT,
  price INT,
  publishDate timestamp,
  counts INT,
  lastUpdate timestamp,
  deleted BOOLEAN,
  FOREIGN KEY (bookId)
  	REFERENCES Book (id)
);

CREATE TABLE Members (
  id SERIAL PRIMARY key,
  memberId INT NOT NULL,
  firstName VARCHAR,
  secondName VARCHAR,
  birthDate timestamp,
  membershipDate timestamp,
  phoneNumber INT,
  address VARCHAR,
  lastUpdate timestamp,
  deleted BOOLEAN
);


CREATE TABLE Loan (
  id SERIAL PRIMARY key,
  loanId INT NOT NULL,
  printId INT not NULL,
  memberId INT,
  loanDate timestamp,
  returnedDate timestamp,
  lastUpdate timestamp,
  deleted BOOLEAN,
  FOREIGN KEY (printId)
  	REFERENCES Print (id),
  FOREIGN KEY (memberId)
  	REFERENCES Members (id)
);


SELECT CURRENT_TIMESTAMP;

INSERT INTO Book (ISBN, title ,description ,publisher , category , languages,  lastUpdate )
VALUES (1, 'Alice', 'story', 'Jack', 'novel','en',current_timestamp);

INSERT INTO Author ( authorId ,  firstName ,  secondName ,  lastUpdate )
VALUES (1, 'tom', 'hall', current_timestamp);

INSERT INTO Book_Author (‌Bookid,authorid,lastupdate)values (
		(SELECT id FROM Book WHERE id = 1),
		(SELECT id FROM Author WHERE id = 1),
		current_timestamp);

INSERT INTO Print (printId ,  bookId ,  versions ,  price,  publishDate ,  counts,  lastUpdate)
VALUES (200,(SELECT id FROM Book WHERE id = 1), 1, 1000, '2010-06-04',100,current_timestamp);	
	

INSERT INTO Members (memberId,firstName ,  secondName ,  birthDate ,  membershipDate ,  phoneNumber ,  address ,  lastUpdate)values (
		100,'Albert','Stephani','2000-06-04','2015-06-04',90000,'Paris'	,current_timestamp);


INSERT INTO Loan (loanId ,  printId ,  memberId,  loanDate ,  lastUpdate)
VALUES (300,(SELECT id FROM Print WHERE id = 1), (SELECT id FROM Members WHERE id = 1),  '2010-06-04',current_timestamp);	
	






	/*

INSERT INTO Author (id ,  firstName ,  secondName ,  lastUpdate)
VALUES (1, 'Alice', 'Tom', '2022-06-03 16:36:26.497 +0430');


INSERT INTO Print (  printId ,  ISBN ,  versions,  price ,  publishDate ,  counts ,  lastUpdate )
VALUES (10, '1', 2,3000, '2021-01-01 11:11:11.111 +0430',3,'2022-06-03 16:36:26.497 +0430');

INSERT INTO Members ( id ,  firstName ,  secondName ,  birthDate ,  membershipDate ,  phoneNumber ,  address ,  lastUpdate)
VALUES (20, 'Mat', 'Blingham', '2000-05-03 16:36:26.497 +0430','2022-05-03 16:36:26.497 +0430',987654321,'London','2022-05-03 16:36:26.497 +0430')

INSERT INTO Loan ( id , printId ,  memberId ,  loanDate  , lastUpdate)
VALUES (3000, 10, 20, '2000-05-03 16:36:26.497 +0430','2022-05-03 17:36:26.497 +0430')


ALTER TABLE Book 
ADD COLUMN Deleted BOOLEAN;

ALTER TABLE Author 
ADD COLUMN Deleted BOOLEAN;

ALTER TABLE Book_Author 
ADD COLUMN Deleted BOOLEAN;

ALTER TABLE Print 
ADD COLUMN Deleted BOOLEAN;

ALTER TABLE Members 
ADD COLUMN Deleted BOOLEAN;

ALTER TABLE Loan 
ADD COLUMN Deleted BOOLEAN;

*/




select  * from  book b  

select  * from  author

select  * from  print p  

select  * from  loan l  

select  * from  members m 

select  * from  book_author



  SELECT * FROM Book_Author WHERE lastUpdate > '0001-01-01 00:00:0 +0430';

/*

delete from Book where id= 1;


SELECT * from Book where lastUpdate=(SELECT MAX(lastUpdate) FROM Book);


delete from  book_author ba 

delete  from  author a 

delete  from  loan l  

delete  from  print p  


delete  from  members m 

delete  from  book b  




DROP SCHEMA public CASCADE;
CREATE SCHEMA public;

*/

delete from  book_author ba where id= 1;
 
delete from Loan where id= 1;


DROP SCHEMA public CASCADE;
CREATE SCHEMA public;

UPDATE Members
SET column1 = value1,
    column2 = value2,
    ...
WHERE condition;


DROP SCHEMA public CASCADE;
CREATE SCHEMA public;

