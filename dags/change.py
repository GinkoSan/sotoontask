from datetime import datetime, timedelta
import json
from airflow import DAG
import requests
from airflow.operators.python import PythonOperator
from airflow.operators.postgres_operator import PostgresOperator
from airflow.hooks.postgres_hook import PostgresHook

def logging(name,id,stat):
    for k in stat:
        if isinstance(stat[k], datetime):
            stat[k] = stat[k].isoformat()

    if stat['deleted']==True:
        stat=None
    requests.post('http://logger:5123/log',json={"type":name,"id":id,"update":stat})


def check():
    src = PostgresHook(postgres_conn_id='postgres_localhost')
    dest = PostgresHook(postgres_conn_id='Slave')
    src_conn = src.get_conn()
    cursor = src_conn.cursor()
    dest_conn = dest.get_conn()
    dest_cursor = dest_conn.cursor()

    cursor.execute("SELECT * FROM Book;")
    fetched=cursor.fetchone()
    if  fetched is None:
        print(fetched)
        print("===== Error src DB ====")
    else:
        print(fetched)
        print("src DB healthy")

    catched= dest_cursor.execute("SELECT * FROM Book;")
    if catched is None:
        print("===== Error dest DB ====")
    else:
        print(catched)
        print("dest DB healthy")



def member():
    src = PostgresHook(postgres_conn_id='postgres_localhost')
    dest = PostgresHook(postgres_conn_id='Slave')
    src_conn = src.get_conn()
    cursor = src_conn.cursor()
    dest_conn = dest.get_conn()
    dest_cursor = dest_conn.cursor()
    
    dest_cursor.execute("SELECT max(lastUpdate) FROM Members;")
    fetched=dest_cursor.fetchone()
    
    if fetched[0] is None:
        lastUpdates = '0001-01-01 00:00:0 +0430'

    else:
        lastUpdates=fetched[0]

    
    cursor.execute(" SELECT * FROM Members WHERE lastUpdate > %s", [lastUpdates])
    dest.insert_rows(table="Members", rows=cursor,replace=True,replace_index="id",target_fields=['id','memberId',  'firstName','secondName' ,
                                                                                                 'birthDate' ,  'membershipDate' ,  'phoneNumber',
                                                                                                 'address' ,  'lastUpdate' ,'deleted'])
    cursor.execute(" SELECT * FROM Members WHERE lastUpdate > %s", [lastUpdates])
    desc = cursor.description
    column_names = [col[0] for col in desc]
    data = [dict(zip(column_names, row) ) for row in cursor.fetchall()]
    for line in data:
        logging('Members',line['id'],line)

    dest_cursor.close()
    cursor.close()


def author():
    src = PostgresHook(postgres_conn_id='postgres_localhost')
    dest = PostgresHook(postgres_conn_id='Slave')
    src_conn = src.get_conn()
    cursor = src_conn.cursor()
    dest_conn = dest.get_conn()
    dest_cursor = dest_conn.cursor()
    
    dest_cursor.execute("SELECT max(lastUpdate) FROM author;")
    fetched=dest_cursor.fetchone()
    if fetched[0] is None:
        lastUpdates = '0001-01-01 00:00:0 +0430'
    else:
        lastUpdates=fetched[0]
    cursor.execute(" SELECT * FROM author WHERE lastUpdate > %s", [lastUpdates])
    dest.insert_rows(table="author", rows=cursor,replace=True,replace_index="id",target_fields=[ 'id','authorId' ,  'firstName' ,  'secondName' , 'lastUpdate' ,'deleted'])
    cursor.execute(" SELECT * FROM author WHERE lastUpdate > %s", [lastUpdates])
    desc = cursor.description
    column_names = [col[0] for col in desc]
    data = [dict(zip(column_names, row) ) for row in cursor.fetchall()]
    for line in data:
        logging('author',line['id'],line)

    dest_cursor.close()
    cursor.close()
    
    
def book():
    src = PostgresHook(postgres_conn_id='postgres_localhost')
    dest = PostgresHook(postgres_conn_id='Slave')
    src_conn = src.get_conn()
    cursor = src_conn.cursor()
    dest_conn = dest.get_conn()
    dest_cursor = dest_conn.cursor()
    
    dest_cursor.execute("SELECT max(lastUpdate) FROM Book;")
    fetched=dest_cursor.fetchone()
    if fetched[0] is None:
        lastupdates = '0001-01-01 00:00:0 +0430'
    else:
        lastupdates=fetched[0]
    cursor.execute("SELECT * FROM Book WHERE lastUpdate > %s", [lastupdates])
    dest.insert_rows(table="Book", rows=cursor,replace=True,replace_index=['id'],target_fields=['id','ISBN', 'title' ,'description' ,'publisher' , 'category' , 'languages' ,  'lastUpdate' ,'translatedFrom','deleted'])
    dest.insert_rows(table="Book", rows=cursor,replace=True,replace_index=['id','ISBN', 'title' ,'description' ,'publisher' , 'category' , 'languages' ,  'lastUpdate' 'translatedFrom','deleted'],target_fields=['id','ISBN', 'title' ,'description' ,'publisher' , 'category' , 'languages' ,  'lastUpdate' ,'translatedFrom','deleted'])
    
    cursor.execute(" SELECT * FROM Book WHERE lastUpdate > %s", [lastupdates])
    desc = cursor.description
    column_names = [col[0] for col in desc]
    data = [dict(zip(column_names, row) ) for row in cursor.fetchall()]
    for line in data:
        logging('Book',line['id'],line)

    dest_cursor.close()
    cursor.close()

    
def bookauthor():
    src = PostgresHook(postgres_conn_id='postgres_localhost')
    dest = PostgresHook(postgres_conn_id='Slave')
    src_conn = src.get_conn()
    cursor = src_conn.cursor()
    dest_conn = dest.get_conn()
    dest_cursor = dest_conn.cursor()
    
    dest_cursor.execute("SELECT max(lastUpdate) FROM Book_Author;")
    fetched=dest_cursor.fetchone()
    print("-------MAXID-----",fetched)
    if fetched[0] is None:
        lastUpdates = '0001-01-01 00:00:0 +0430'
    else:
        lastUpdates=fetched[0]
    cursor.execute("SELECT * FROM Book_Author WHERE lastUpdate > %s", [lastUpdates])
    #dest.insert_rows(table="Book_Author", rows=cursor,replace=True,replace_index='id',target_fields=[ 'id','bookId','authorId',
    #                                                                                                  'lastUpdate','deleted'])
    dest.insert_rows(table="Book_Author", rows=cursor)
    
    cursor.execute(" SELECT * FROM Book_Author WHERE lastUpdate > %s", [lastUpdates])
    desc = cursor.description
    column_names = [col[0] for col in desc]
    data = [dict(zip(column_names, row) ) for row in cursor.fetchall()]
    for line in data:
        logging('Book_Author',line['id'],line)

    dest_cursor.close()
    cursor.close()
    
def prints():
    src = PostgresHook(postgres_conn_id='postgres_localhost')
    dest = PostgresHook(postgres_conn_id='Slave')
    src_conn = src.get_conn()
    cursor = src_conn.cursor()
    dest_conn = dest.get_conn()
    dest_cursor = dest_conn.cursor()
    
    dest_cursor.execute("SELECT max(lastUpdate) FROM Print;")
    fetched=dest_cursor.fetchone()
    print("-------MAXID-----",fetched)
    if fetched[0] is None:
        lastUpdates = '0001-01-01 00:00:0 +0430'
    else:
        lastUpdates=fetched[0]
    cursor.execute("SELECT * FROM Print WHERE lastUpdate > %s", [lastUpdates])
    dest.insert_rows(table="Print", rows=cursor,replace=True,replace_index="id",target_fields=['id','printId' ,  'bookId',  'versions' ,  'price',  'publishDate' ,  'counts', 'lastUpdate' ,'deleted'])
    cursor.execute(" SELECT * FROM Print WHERE lastUpdate > %s", [lastUpdates])
    desc = cursor.description
    column_names = [col[0] for col in desc]
    data = [dict(zip(column_names, row) ) for row in cursor.fetchall()]
    for line in data:
        logging('Print',line['id'],line)

    dest_cursor.close()
    cursor.close()

def loan():
    src = PostgresHook(postgres_conn_id='postgres_localhost')
    dest = PostgresHook(postgres_conn_id='Slave')
    src_conn = src.get_conn()
    cursor = src_conn.cursor()
    dest_conn = dest.get_conn()
    dest_cursor = dest_conn.cursor()
    
    dest_cursor.execute("SELECT max(lastUpdate) FROM Loan;")
    fetched=dest_cursor.fetchone()
    print("-------MAXID-----",fetched)
    if fetched[0] is None:
        lastUpdates = '0001-01-01 00:00:0 +0430'
    else:
        lastUpdates=fetched[0]
    cursor.execute("SELECT * FROM Loan WHERE lastUpdate > %s", [lastUpdates])
    dest.insert_rows(table="Loan", rows=cursor,replace=True,replace_index="id",target_fields=['id','loanId' ,  'printId' ,  'memberId',  'loanDate'  ,'returnedDate', 'lastUpdate' ,'deleted'])
    cursor.execute(" SELECT * FROM Loan WHERE lastUpdate > %s", [lastUpdates])
    desc = cursor.description
    column_names = [col[0] for col in desc]
    data = [dict(zip(column_names, row) ) for row in cursor.fetchall()]
    for line in data:
        logging('Loan',line['id'],line)

    dest_cursor.close()
    cursor.close()


with DAG(
    dag_id="changesV5",
    start_date=datetime(2022, 6, 2),
    schedule_interval=None
) as dag:

    task1 = PythonOperator(
        task_id="checkconnection",
        python_callable=check
    )

    task2 = PythonOperator(
        task_id="member",
        python_callable=member
    ) 
    task3 = PythonOperator(
        task_id="author",
        python_callable=author
    ) 
    task4 = PythonOperator(
        task_id="book",
        python_callable=book
    ) 
    task5 = PythonOperator(
        task_id="bookauthor",
        python_callable=bookauthor
    ) 
    task6 = PythonOperator(
        task_id="prints",
        python_callable=prints
    ) 
    task7 = PythonOperator(
        task_id="loan",
        python_callable=loan
    ) 


#UPDATE/INSERT DAG
task1 >> task2 >> task7
task1 >> task3 >> task5
task1 >> task4 >> task5
task4 >> task6 >> task7

#DELETE DAG
# task14 >> task9
# task14 >> task13
# task12 >> task10
# task12 >> task11