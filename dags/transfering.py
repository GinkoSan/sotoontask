from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.operators.postgres_operator import PostgresOperator
from airflow.hooks.postgres_hook import PostgresHook

def updatingg():
    src = PostgresHook(postgres_conn_id='postgres_localhost')
    dest = PostgresHook(postgres_conn_id='Slave')
    src_conn = src.get_conn()
    cursor = src_conn.cursor()
    dest_conn = dest.get_conn()
    dest_cursor = dest_conn.cursor()
    #dest_cursor.execute("CREATE TABLE if not exists Book (  ISBN INT NOT NULL,  title VARCHAR,  description VARCHAR,  publisher VARCHAR,  category VARCHAR,  languages VARCHAR,  lastUpdate timestamp,  translatedFrom INT ,  PRIMARY KEY (ISBN),  FOREIGN KEY (translatedFrom)      REFERENCES Book (ISBN));")
    
    dest_cursor.execute("SELECT MAX‍(ID) FROM Book;")
    MAXID = dest_cursor.fetchone()[0]
    dest_cursor.close()
    if MAXID is None:
        MAXID = 0
    cursor.execute("SELECT * FROM Book WHERE ID > %s", [MAXID])
    print("-------lastUpdate-----",lastUpdate)
    dest.insert_rows(table="Book", rows=cursor)
    
    dest_cursor.execute("SELECT MAX(lastUpdate) FROM Book;")
    lastUpdate = dest_cursor.fetchone()[0]
    dest_cursor.close()
    if lastUpdate is None:
        lastUpdate = 0
    cursor.execute("SELECT * FROM Book WHERE lastUpdate > %s", [lastUpdate])
    print("-------lastUpdate-----",lastUpdate)
    dest.UPSERT(table="Book", rows=cursor)


"""def check_table_exist(sql_to_get_schema, sql_to_check_table_exist,
                          table_name):
# callable function to get schema name and after that check if table exist 
        hook = PostgresHook()
        # get schema name
        query = hook.get_records(sql=sql_to_get_schema)
        for result in query:
            if 'airflow' in result:
                schema = result[0]
                print(schema)
                break

        # check table exist
        query = hook.get_first(sql=sql_to_check_table_exist.format(schema, table_name))
        print(query)
        if query:
            return True
        else:
            raise ValueError("table {} does not exist".format(table_name))
""" 

with DAG(
    dag_id="TransferingV3",
    start_date=datetime(2022, 6, 2),
    schedule_interval=None
) as dag:



    task1 = PythonOperator(
        task_id="updatingg",
        python_callable=updatingg
    )

    task2 = PostgresOperator(
        task_id='insert_into_book',
        postgres_conn_id='postgres_localhost',
        sql="""
           INSERT INTO Book (ISBN, title ,description ,publisher , category , languages,  lastUpdate )
            VALUES (6, 'Chemistry', 'Academic', 'Science', 'Scientific','en','2022-06-04 21:31:17.263 +0430')
        """
    )
    task3 = PostgresOperator(
        task_id='delete_data_from_table',   
        postgres_conn_id='postgres_localhost',
        sql="""
            delete from Book where ISBN= 5;
        """
    )
#    task4 = PythonOperator(
#    task_id='create_if_needed'
#    python_callable=check_table_exist
#   )

task2 >> task1