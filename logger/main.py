from flask import Flask, request
from datetime import datetime
import json

logFile = open("data.log", "a")
app = Flask(__name__)


@app.route("/log", methods=["POST"])
def hello_world():
    l = request.get_json()

    if "type" not in l:
        return "no type", 400
    if "id" not in l:
        return "no id", 400
    if "update" not in l:
        return "no update", 400
    if l["update"] is not None and not isinstance(l["update"], dict):
        return "bad update", 400
    if l["update"] is not None and not all(isinstance(k, str) for k in l["update"]):
        return "bad update key", 400

    logFile.write(datetime.now().isoformat() + ": " + json.dumps(l).replace("\n", "\\n") + "\n")
    logFile.flush()
    return "", 200


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5123)
    logFile.close()
